<?php

namespace Tests\Unit;

use App\Models\Book;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;


class BookTest extends TestCase
{
    use WithFaker;

    /** @test */
    public function it_can_created(): void
    {
        $data = [
            'name' => $this->faker->name,
            'isbn' => rand(3, 6),
            'value' => null,
        ];
      
        $bookModel = new Book();
        $book = $bookModel->create($data);
      
        $this->assertInstanceOf(Book::class, $book);
        $this->assertEquals($data['name'], $book->name);
        $this->assertEquals($data['isbn'], $book->isbn);
        $this->assertEquals($data['value'], $book->value);
    }
}

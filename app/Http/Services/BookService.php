<?php

namespace App\Http\Services;

use App\Models\Book;
use App\Http\Requests\Book\StoreRequest;
use App\Http\Requests\Book\UpdateRequest;
use App\Http\Services\ResponseService;


class BookService
{
    protected ResponseService $responseService;

    public function __construct()
    {
        $this->responseService = new ResponseService();
    }

    public function store(StoreRequest $request):\Illuminate\Http\JsonResponse
    {
        $book = Book::create([
            'name' => $request->name,
            'isbn' => $request->isbn,
            'value' => $request->value,
        ]);

        if (!$book) return $this->responseService->defaultErrorReturn('The book was not created.');

        return $this->responseService->defaultSuccessReturn('The book was created!',['data' => $book]);
    }

    public function show(string $id):\Illuminate\Http\JsonResponse
    {   
        $book = Book::find($id);
        
        if (!$book) return $this->responseService->defaultErrorReturn('The book was not found.', 404);
     
        return $this->responseService->defaultSuccessReturn('Book found.',['data' => $book]);
    }

    public function update(UpdateRequest $request, string $id):\Illuminate\Http\JsonResponse
    {
        $book = Book::where('id', $id)->update(['name' => $request->name, 'isbn' => $request->isbn, 'value' => $request->value]);

        if (!$book) return $this->responseService->defaultErrorReturn('The book was not updated.');

        return $this->responseService->defaultSuccessReturn("The book was updated!");
    }

    public function destroy(string $id):\Illuminate\Http\JsonResponse
    {
        $book = Book::where('id', $id)->delete();
       
        if (!$book) return $this->responseService->defaultErrorReturn('The book was not deleted.');

        return $this->responseService->defaultSuccessReturn("The book was deleted!");
    }

}
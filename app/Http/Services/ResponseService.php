<?php

namespace App\Http\Services;


class ResponseService
{
    public function defaultSuccessReturn(string $message = '', array $params = []):\Illuminate\Http\JsonResponse
    {
        $responseArray = [
            'sucess'  => true,
            'message' => $message,
        ];

        if(!empty($params)){
            foreach ($params as $key => $value) {
                $responseArray[$key] = $value;
            }
        }
        return response()->json($responseArray, 200);
    }

    public function defaultErrorReturn(string $message = '', int $code = 400, array $params = []):\Illuminate\Http\JsonResponse
    {
        $responseArray = [
            'sucess'  => false,
            'message' => $message,
        ];

        if(!empty($params)){
            foreach ($params as $key => $value) {
                $responseArray[$key] = $value;
            }
        }

        return response()->json($responseArray, $code);
    }
    
}
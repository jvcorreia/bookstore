<?php

namespace App\Http\Services;

use App\Models\User;
use App\Http\Requests\Auth\RegisterRequest;
use App\Http\Requests\Auth\LoginRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;


class AuthService
{

    protected ResponseService $responseService;

    public function __construct()
    {
        $this->responseService = new ResponseService();
    }

    public function login(LoginRequest $request): \Illuminate\Http\JsonResponse
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            $user = Auth::user();

            return $this->responseService->defaultSuccessReturn("Logged.", ['user' => $user, 'token' => $user->createToken('ApiToken')->plainTextToken]);
        }

            return $this->responseService->defaultErrorReturn("Invalid e-mail or password!");
    }

    public function register(RegisterRequest $request): \Illuminate\Http\JsonResponse
    {

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        if ($user) {
            return $this->responseService->defaultSuccessReturn("User created!", ['user' => $user]);
        }

            return $this->responseService->defaultErrorReturn("User not created.");
    }

    public function logout(): \Illuminate\Http\JsonResponse
    {
        $logout = Auth::user()->tokens()->delete();

        if ($logout) {
            return $this->responseService->defaultSuccessReturn("Logged out.");
        }

            return $this->responseService->defaultErrorReturn("Error on Log out.");
    }

}
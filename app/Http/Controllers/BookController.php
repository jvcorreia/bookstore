<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Http\Requests\Book\StoreRequest;
use App\Http\Requests\Book\UpdateRequest;
use App\Http\Services\BookService;

class BookController extends Controller
{

    protected BookService $bookService;

    public function __construct()
    {
        $this->middleware('auth:sanctum');
        $this->bookService = new BookService();
    }

    public function store(StoreRequest $request):\Illuminate\Http\JsonResponse
    {
        return $this->bookService->store($request);
    }

    public function show(string $id):\Illuminate\Http\JsonResponse
    {   
        return $this->bookService->show($id);
    }

    public function update(UpdateRequest $request, string $id):\Illuminate\Http\JsonResponse
    {
        return $this->bookService->update($request, $id);
    }

    public function destroy(string $id):\Illuminate\Http\JsonResponse
    {
        return $this->bookService->destroy($id);
    }
}

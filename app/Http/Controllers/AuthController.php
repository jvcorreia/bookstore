<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Http\Requests\Auth\RegisterRequest;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Controllers\Controller;
use App\Http\Services\AuthService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;


class AuthController extends Controller
{
    protected AuthService $authService;

    public function __construct()
    {
        $this->middleware('auth:sanctum', ['except' => ['login', 'register']]);
        $this->authService = new AuthService();
    }

    public function login(LoginRequest $request): \Illuminate\Http\JsonResponse
    {
       return $this->authService->login($request);
    }

    public function register(RegisterRequest $request): \Illuminate\Http\JsonResponse
    {
        return $this->authService->register($request);
    }

    public function logout(): \Illuminate\Http\JsonResponse
    {
        return $this->authService->logout();
    }


}
